### dicey: a diceware password generator.

diceware: http://world.std.com/~reinhold/diceware.html

It's recommended to use a virtualenv in project directory (`pip install virtualenv`):

    virtualenv venv

Activate the *venv*:

    . venv/bin/activate

Install `dicey`:

    pip install --editable .

(Note: the editable option makes it so you can change source code.)

Now `dicey` is runnable in the commandline, try it out!

    dicey

Built with:

  - Click: http://click.pocoo.org/5/

Testing:

  - Used `pytest`: `pip install pytest`
    - Run, `py.test tests/` in the root directory
  - For coverage: `pip install pytest-cov`
    - Run, `py.test --cov=tests tests/`
