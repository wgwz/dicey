import pytest
from click.testing import CliRunner
from .context import dicey

@pytest.fixture(scope='function')
def runner(request):
  return CliRunner()

def test_diceword(runner):
  result = runner.invoke(dicey.cli, ['diceword'])
  assert result.exit_code == 0

def test_get_pswd(runner):
  result = runner.invoke(dicey.cli, ['get_pswd', '5'])
  assert result.exit_code == 0
