from .context import dicey

#from context.dicey import dice_roll, generate_key, dice_word, password_list

def test_dice_roll():
  assert dicey.dice_roll() in [str(i+1) for i in range(6)]

def test_generate_key_is_string():
  assert isinstance(dicey.generate_key(), str)

def test_generate_key_is_valid():
  key = dicey.generate_key()
  for number in key:
    assert number in [str(i+1) for i in range(6)]

def test_dice_word():
  assert isinstance(dicey.dice_word(), str)

def test_password_list_length():
  assert len(dicey.password_list(5)) == 5

def test_password_list_is_strings():
  for each_password in dicey.password_list(5):
    assert isinstance(each_password, str)
