from .context import dicey

def test_dice_dict_keys_integers():
  for _ in dicey.dice_dict.keys():
    assert isinstance(int(_), int)
