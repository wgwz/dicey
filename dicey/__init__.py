from .dicey import cli
from .app import dice_roll, generate_key, dice_word, password_list
from .parse import dice_dict
