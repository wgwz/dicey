#!/usr/bin/env python
import click
import os

path_to_list = os.path.abspath(os.path.join(os.path.dirname(__file__), 'list.txt'))

with click.open_file(path_to_list, 'r') as dice_ware:
  dice_dict = dict([_.split() for _ in dice_ware.readlines()])
