#!/usr/bin/env python
from app import dice_word, password_list
import click

@click.group()
def cli():
  pass

@cli.command()
def diceword():
  click.echo(dice_word())

@cli.command()
@click.argument('num_words')
def get_pswd(num_words):
  click.echo(password_list(int(num_words)))
