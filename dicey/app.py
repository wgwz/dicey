#!/usr/bin/env python
from random import SystemRandom
from parse import dice_dict

DICE_LOW = 1
DICE_HIGH = 6
KEY_LENGTH = len(dice_dict.keys()[0])
random = SystemRandom()

def dice_roll():
  return str(random.randint(DICE_LOW, DICE_HIGH))

def generate_key():
  key = ''
  for i in range(KEY_LENGTH):
    key += dice_roll()
  return key

def dice_word():
  return dice_dict[generate_key()]

def password_list(num_words):
  password_list = []
  for i in range(num_words):
    password_list.append(dice_word())
  return password_list
