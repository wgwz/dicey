from setuptools import setup, find_packages

setup(
  name='dicey',
  version='0.0.1',
  packages=find_packages(exclude=('tests')),
  include_package_data=True,
  install_requires=[
    'Click',
  ],
  entry_points='''
    [console_scripts]
    dicey=dicey.dicey:cli
  ''',
  zip_safe=False,
)
